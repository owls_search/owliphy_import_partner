
from pytest import raises
from owliphy_import_partners.main import OwliphyImportServiceTest

def test_owliphy_import_partners():
    # test owliphy_import_partners without any subcommands or arguments
    with OwliphyImportServiceTest() as app:
        app.run()
        assert app.exit_code == 0


def test_owliphy_import_partners_debug():
    # test that debug mode is functional
    argv = ['--debug']
    with OwliphyImportServiceTest(argv=argv) as app:
        app.run()
        assert app.debug is True


def test_command1():
    # test command1 without arguments
    argv = ['command1']
    with OwliphyImportServiceTest(argv=argv) as app:
        app.run()
        data,output = app.last_rendered
        assert data['foo'] == 'bar'
        assert output.find('Foo => bar')


    # test command1 with arguments
    argv = ['command1', '--foo', 'not-bar']
    with OwliphyImportServiceTest(argv=argv) as app:
        app.run()
        data,output = app.last_rendered
        assert data['foo'] == 'not-bar'
        assert output.find('Foo => not-bar')
