
from setuptools import setup, find_packages
from owliphy_import_partners.core.version import get_version

VERSION = get_version()

f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name='owliphy_import_partners',
    version=VERSION,
    description='Service to import owliphy partners feeds',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Mikhail Starikov',
    author_email='michail.a.starikov@gmail.com',
    url='https://gitlab.com/owls_search/owliphy_import_partner',
    license='unlicensed',
    packages=find_packages(exclude=['ez_setup', 'tests*']),
    package_data={'owliphy_import_partners': ['templates/*']},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        owliphy_import_partners = owliphy_import_partners.main:main
    """,
)
