# Service to import owliphy partners feeds

## Installation

```
$ pip install -r requirements.txt

$ pip install setup.py
```

## Development

This project includes a number of helpers in the `Makefile` to streamline common development tasks.

### Environment Setup

The following demonstrates setting up and working with a development environment:

```
### create a virtualenv for development

$ make virtualenv

$ source env/bin/activate


### run owliphy_import_partners cli application

$ owliphy_import_partners --help


### run pytest / coverage

$ make test
```
## env vars
```
mcedit /etc/environment 
DB_HOST=localhost
DB_USERNAME=username
DB_PASSWORD=password
DB_DATABASE=owliphy
DB_PORT=3306
```
OR
```ini
db.ini

[connection]
DB_HOST = localhost
DB_USERNAME = root
DB_PASSWORD = 3j5kavcev
DB_DATABASE = owliphy
DB_PORT = 3306
```
## Cron job
```
crontab -e
0   4    *   *   *   /bin/bash -c ~/owliphy_import_partner/cron_commands/all-partners.sh
0   10   *   *   *   /bin/bash -c ~/owliphy_import_partner/cron_commands/all-partners.sh
0   15   *   *   *   /bin/bash -c ~/owliphy_import_partner/cron_commands/all-partners.sh
0   0    *   *   *   /bin/bash -c ~/owliphy_import_partner/cron_commands/all-partners.sh
```