import urllib.request
from .write_file import WriteFile


class Downloader:

    maximum_timeout_seconds = 60

    def __init__(self, url, output_info, partner, file_name):
        self.url = url
        self.file_name = file_name
        self.partner = partner
        self.output_info = output_info

    def download(self):
        url_obj = urllib.request.urlopen(self.url, timeout = self.maximum_timeout_seconds)
        meta = url_obj.info()

        if meta['Content-Length']:
            self.output_info.file_size_mb = (int(meta['Content-Length']) / 1024 / 1024)
        else:
            self.output_info.file_size_mb = 0

        yield self.output_info
        importer = WriteFile(self.partner, self.file_name)

        while True:
            buffer = url_obj.read(importer.buffer_size)
            if not buffer:
                break

            importer.write(buffer)
            if self.output_info.file_size_mb > 0:
                self.output_info.download_progress_percent = importer.current_file_size * 100. / self.output_info.file_size_mb

            self.output_info.downloaded_mb = importer.current_file_size / 1024 / 1024
            yield self.output_info
        importer.close()
        yield self.output_info
        return
