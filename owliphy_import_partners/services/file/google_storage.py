import datetime
import os
from google.cloud import storage
import six


class GoogleStorage:
    project = 'reliable-proton-251017'

    def __init__(self, bucket):
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '%s/google-services-key.json' % os.getcwd()
        self.bucket = bucket
        self.client = storage.Client(self.project)

    @staticmethod
    def safe_filename(filename):
        date = datetime.datetime.utcnow().strftime("%Y-%m-%d-%H%M%S")
        basename, extension = filename.rsplit('.', 1)
        return "{0}-{1}.{2}".format(basename, date, extension)

    def list_files(self):
        blobs = self.client.list_blobs(self.bucket)
        for blob in blobs:
            yield blob

    def load_file(self, name, destination_name):
        bucket = self.client.bucket(self.bucket)
        blob = bucket.blob(name)
        blob.download_to_filename(destination_name)

    def upload_file(self, file_stream, filename, content_type):
        bucket = self.client.bucket(self.bucket)
        blob = bucket.blob(filename)
        if blob.exists(self.client):
            blob.delete(self.client)

        blob.upload_from_string(
            file_stream,
            content_type=content_type)

        url = blob.public_url

        if isinstance(url, six.binary_type):
            url = url.decode('utf-8')

        return url
