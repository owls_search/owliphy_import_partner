import os
from .xml_reader import XMLReader
from ..job_offer import JobOffer
from ..util import has_not_empty_prop, generate_mini_description
from ..db_time_converter import DbTimeConverter
from .partner_xml_reader import PartnerXmlReader


class StellenanzeigenXmlReader(PartnerXmlReader):

    def __init__(self, file):
        self.file = file
        self.job_element = 'job'

        self.child_elements = [
            'ID',
            'LOGO',
            'POSITION',
            'UNTERNEHMEN',
            'VOLLTEXT',
            'URL',
            'DATUM',
            'EINSATZORTTEXT',
            'RUBRIKEN',
            'KEYWORDS'
        ]

        self.job_attrs_mapping = {
            'ID': 'external_id',
            'POSITION': 'title',
            'UNTERNEHMEN': 'company_name',
            'LOGO': 'logo',
            'URL': 'url',
            'EINSATZORTTEXT': 'location',
        }

    def read(self, output_info):
        output_info.file_name = self.file
        output_info.file_size_mb = os.path.getsize(
            '%s/%s/%s' % ('./import_files', 'stellenanzeigen', self.file)) / 1024 / 1024

        for element in self._read_elements(self.file):
            yield element

    def _read_elements(self, file):
        reader = XMLReader('stellenanzeigen', file)
        reader.start(self.job_element, self.child_elements)
        for offer_raw_data in reader.next_element():

            job_offer = JobOffer()

            job_offer.set('dynamic_bid', 0.1)

            for prop, data in offer_raw_data.items():
                if prop in self.job_attrs_mapping.keys():
                    try:
                        job_offer.set(self.job_attrs_mapping[prop], data)
                    except (AttributeError, TypeError):
                        raise RuntimeError('%s doesn`t have %s method' % (JobOffer.__name__, prop))

            job_offer.set('work_type', 'full')
            if has_not_empty_prop(offer_raw_data, 'DATUM'):
                job_offer.set('starts_from', DbTimeConverter.convert(offer_raw_data['DATUM']))

            if has_not_empty_prop(offer_raw_data, 'VOLLTEXT'):
                description = offer_raw_data['VOLLTEXT']
                keywords = offer_raw_data['KEYWORDS'] if has_not_empty_prop(offer_raw_data, 'KEYWORDS') else ''

                job_offer.set('html_description', description)
                job_offer.set('description', '%s %s' % (description, keywords))
                job_offer.set('mini_description', generate_mini_description(description))

            yield job_offer
