import os
from .xml_reader import XMLReader
from ..job_offer import JobOffer
from ..util import has_not_empty_prop, get_plain_description, generate_mini_description, is_it_job
from .partner_xml_reader import PartnerXmlReader
from ..db_time_converter import DbTimeConverter


class NeuvooXmlReader(PartnerXmlReader):

    def __init__(self, files):
        self.files = files
        self.job_element = 'job'

        self.child_elements = [
            'jobid',
            'title',
            'company',
            'logo',
            'description',
            'url',
            'date',
            'cpc',
            'city',
            'state',
            'country',
            'salary'
        ]
        self.job_attrs_mapping = {
            'jobid': 'external_id',
            'title': 'title',
            'company': 'company_name',
            'logo': 'logo',
            'url': 'url',
            'cpc': 'dynamic_bid',
            'salary': 'salary_from',
        }

    @staticmethod
    def set_location(raw_offer, job_offer):
        if has_not_empty_prop(raw_offer, 'city'):
            location = raw_offer['city']
        elif has_not_empty_prop(raw_offer, 'state'):
            location = raw_offer['state']
        elif has_not_empty_prop(raw_offer, 'country'):
            location = raw_offer['country']
        else:
            location = 'Deutschland'

        job_offer.set('location', location)

    def read(self, output_info):
        for file in self.files:
            output_info.file_name = file
            output_info.file_size_mb = os.path.getsize(
                '%s/%s/%s' % ('./import_files', 'neuvoo', file)) / 1024 / 1024

            for element in self._read_elements(file, output_info):
                yield element

    def _is_appropriate_job(self, offer_raw_data):
        return \
            has_not_empty_prop(offer_raw_data, 'description') \
            and (float(offer_raw_data['cpc']) > 0.2 or is_it_job(offer_raw_data['title']) or '€' in offer_raw_data['title'])

    def _read_elements(self, file, output_info):
        reader = XMLReader('neuvoo', file)
        reader.start(self.job_element, self.child_elements)
        for offer_raw_data in reader.next_element():
            if has_not_empty_prop(offer_raw_data, 'salary'):
                output_info.items_with_salary += '%s - %s,' % (offer_raw_data['jobid'], offer_raw_data['salary'])

            if not self._is_appropriate_job(offer_raw_data):
                output_info.filtered_items += 1
                continue

            job_offer = JobOffer()
            for prop, data in offer_raw_data.items():
                if prop in self.job_attrs_mapping.keys():
                    try:
                        job_offer.set(self.job_attrs_mapping[prop], data)
                    except (AttributeError, TypeError):
                        raise RuntimeError('%s doesn`t have %s method' % (JobOffer.__name__, prop))

            self.set_location(offer_raw_data, job_offer)

            job_offer.set('work_type', 'full')
            if has_not_empty_prop(offer_raw_data, 'date'):
                job_offer.set('starts_from', DbTimeConverter.convert(offer_raw_data['date']))

            if has_not_empty_prop(offer_raw_data, 'description'):
                description = offer_raw_data['description']
                plain_description = get_plain_description(description)

                if len(description) > 25000 and len(plain_description) > 0:
                    job_offer.set(
                        'html_description',
                        '%s...' % plain_description[:20000] if len(plain_description) > 20000 else plain_description
                    )
                else:
                    job_offer.set('html_description', description)

                job_offer.set('description', plain_description if len(plain_description) > 0 else description)
                job_offer.set('mini_description', generate_mini_description(plain_description))

            yield job_offer
