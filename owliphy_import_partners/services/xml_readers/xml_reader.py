from lxml import etree


class XMLReader:

    def __init__(self, partner_name, file_name):
        self.imports_dir = './import_files'
        self.file = '%s/%s/%s' % (self.imports_dir, partner_name, file_name)
        self.read_tree = etree
        self.context = {}
        self.job_element = 'job'
        self.required_childs = []

    def start(self, job_element, required_childs):
        self.context = self.read_tree.iterparse(
            self.file,
            dtd_validation=False,
            events=("start", "end"),
            remove_comments=True
        )

        self.job_element = job_element
        self.required_childs = required_childs

    @staticmethod
    def clear_element(element):
        element.clear()
        while element.getprevious() is not None:
            del element.getparent()[0]

    def extract_jobs(self):
        _, root = next(self.context)
        start_tag = None

        for event, element in self.context:
            if element.tag == self.job_element \
                    and event == 'start' \
                    and start_tag is None:
                start_tag = element.tag
            if event == 'end' and element.tag == start_tag:
                yield element
                start_tag = None
                self.clear_element(root)

    def next_element(self):
        for offer_counter, element in enumerate(self.extract_jobs()):
            offer_data = {}
            for data_item in self.required_childs:
                data = element.find(data_item)
                if data is not None:
                    offer_data[data_item] = data.text

            if offer_data:
                yield offer_data
