import os
from .xml_reader import XMLReader
from ..job_offer import JobOffer
from ..util import has_not_empty_prop, generate_mini_description
from ..db_time_converter import DbTimeConverter
from .partner_xml_reader import PartnerXmlReader


class StellenonlinetXmlReader(PartnerXmlReader):

    def __init__(self, file):
        self.file = file
        self.job_element = 'job'

        self.child_elements = [
            'id',
            'title',
            'company',
            'description',
            'url',
            'date',
            'logo',
            'location_text',
        ]

        self.job_attrs_mapping = {
            'id': 'external_id',
            'company': 'company_name',
            'title': 'title',
            'url': 'url',
            'logo': 'logo',
            'location_text': 'location',
        }

    def read(self, output_info):

        output_info.file_name = self.file
        output_info.file_size_mb = os.path.getsize(
            '%s/%s/%s' % ('./import_files', 'stellenonline', self.file)) / 1024 / 1024

        for element in self._read_elements(self.file, output_info):
            yield element

    # No filtering on stellenonline, to keep filter only on ES import side
    def _read_elements(self, file, output_info):
        reader = XMLReader('stellenonline', file)

        reader.start(self.job_element, self.child_elements)
        for offer_raw_data in reader.next_element():
            job_offer = JobOffer()
            for prop, data in offer_raw_data.items():
                if prop in self.job_attrs_mapping.keys():
                    try:
                        job_offer.set(self.job_attrs_mapping[prop], data)
                    except (AttributeError, TypeError):
                        raise RuntimeError('%s doesn`t have %s method' % (JobOffer.__name__, prop))

            job_offer.set('work_type', 'full')
            job_offer.set('dynamic_bid', 0.30)
            if has_not_empty_prop(offer_raw_data, 'date'):
                job_offer.set('starts_from', DbTimeConverter.convert(offer_raw_data['date']))

            if has_not_empty_prop(offer_raw_data, 'description'):
                description = offer_raw_data['description']

                if len(description) > 25000:
                    job_offer.set('html_description', '%s...' % description[:20000])
                else:
                    job_offer.set('html_description', description)
                job_offer.set('description', description)
                job_offer.set('mini_description', generate_mini_description(description))

            yield job_offer
