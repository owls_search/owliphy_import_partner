import os
from .xml_reader import XMLReader
from ..job_offer import JobOffer
from ..util import has_not_empty_prop, generate_mini_description, is_it_job
from ..db_time_converter import DbTimeConverter
from .partner_xml_reader import PartnerXmlReader


class JobliftXmlReader(PartnerXmlReader):

    def __init__(self, file):
        self.file = file
        self.job_element = 'job'

        self.child_elements = [
            'id',
            'title',
            'company',
            'fullDescription',
            'url',
            'publishDate',
            'pricing/cpc',
            'locations/location/city',
            'salary/from',
            'salary/to',
        ]

        self.job_attrs_mapping = {
            'id': 'external_id',
            'company': 'company_name',
            'pricing/cpc': 'dynamic_bid',
            'fullDescription': 'description',
            'title': 'title',
            'url': 'url',
            'salary/from': 'salary_from',
            'salary/to': 'salary_to',
            'locations/location/city': 'location',

        }

    def read(self, output_info):
        output_info.file_name = self.file
        output_info.file_size_mb = os.path.getsize(
            '%s/%s/%s' % ('./import_files', 'joblift', self.file)) / 1024 / 1024

        for element in self._read_elements(self.file, output_info):
            yield element

    def _is_appropriate_job(self, offer_raw_data):
        return \
            has_not_empty_prop(offer_raw_data, 'fullDescription') \
            and (float(offer_raw_data['pricing/cpc']) > 0.2 or is_it_job(offer_raw_data['title']) or '€' in offer_raw_data['title'])

    def _read_elements(self, file, output_info):
        reader = XMLReader('joblift', file)
        reader.start(self.job_element, self.child_elements)
        for offer_raw_data in reader.next_element():
            if not self._is_appropriate_job(offer_raw_data):
                output_info.filtered_items += 1
                continue

            job_offer = JobOffer()
            for prop, data in offer_raw_data.items():
                if prop in self.job_attrs_mapping.keys():
                    try:
                        job_offer.set(self.job_attrs_mapping[prop], data)
                    except (AttributeError, TypeError):
                        raise RuntimeError('%s doesn`t have %s method' % (JobOffer.__name__, prop))

            # < titleCategory_function > it < / titleCategory_function >
            # < titleCategory_function > bussiness < / titleCategory_function >

            job_offer.set('work_type', 'full')
            job_offer.set('logo', 'https://storage.googleapis.com/company-public/joblift.jpg')
            if has_not_empty_prop(offer_raw_data, 'date'):
                job_offer.set('starts_from', DbTimeConverter.convert(offer_raw_data['publishDate']))

            if has_not_empty_prop(offer_raw_data, 'fullDescription'):
                description = offer_raw_data['fullDescription']

                if len(description) > 25000:
                    job_offer.set('html_description', '%s...' % description[:20000])
                else:
                    job_offer.set('html_description', description)
                job_offer.set('description', description)
                job_offer.set('mini_description', generate_mini_description(description))

            yield job_offer
