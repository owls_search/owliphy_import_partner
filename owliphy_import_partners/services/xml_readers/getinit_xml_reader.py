import os
from .xml_reader import XMLReader
from ..job_offer import JobOffer
from ..util import has_not_empty_prop, generate_mini_description
from ..db_time_converter import DbTimeConverter
from .partner_xml_reader import PartnerXmlReader


class GetInItXmlReader(PartnerXmlReader):

    def __init__(self, file):
        self.file = file
        self.job_element = 'job'

        self.child_elements = [
            'id',
            'date',
            'title',
            'url',
            'company',
            'description_html',
            'description_plain',
            'location',
            'keywords',
        ]

        self.job_attrs_mapping = {
            'id': 'external_id',
            'title': 'title',
            'company': 'company_name',
            'url': 'url',
            'location': 'location',
            'description_html': 'html_description',
        }

    def read(self, output_info):
        output_info.file_name = self.file
        output_info.file_size_mb = os.path.getsize(
            '%s/%s/%s' % ('./import_files', 'getinit', self.file)) / 1024 / 1024

        for element in self._read_elements(self.file, output_info):
            yield element

    def _is_appropriate_job(self, offer_raw_data):
        return has_not_empty_prop(offer_raw_data, 'description_html')

    def _read_elements(self, file, output_info):
        reader = XMLReader('getinit', file)
        reader.start(self.job_element, self.child_elements)
        for offer_raw_data in reader.next_element():
            if not self._is_appropriate_job(offer_raw_data):
                output_info.filtered_items += 1
                continue

            job_offer = JobOffer()

            for prop, data in offer_raw_data.items():
                if prop in self.job_attrs_mapping.keys():
                    try:
                        job_offer.set(self.job_attrs_mapping[prop], data)
                    except (AttributeError, TypeError):
                        raise RuntimeError('%s doesn`t have %s method' % (JobOffer.__name__, prop))

            job_offer.set('logo', 'https://storage.googleapis.com/company-public/getinit_logo_grau.png')
            job_offer.set('work_type', 'full')
            job_offer.set('dynamic_bid', 0.15)
            if has_not_empty_prop(offer_raw_data, 'date'):
                job_offer.set('starts_from', DbTimeConverter.convert(offer_raw_data['date']))

            if has_not_empty_prop(offer_raw_data, 'description_plain'):
                description = offer_raw_data['description_plain']
                keywords = offer_raw_data['keywords'] if has_not_empty_prop(offer_raw_data, 'keywords') else ''

                job_offer.set('description', '%s %s' % (description, keywords))
                job_offer.set('mini_description', generate_mini_description(description))

            yield job_offer
