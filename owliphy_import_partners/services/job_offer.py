

class JobOffer:

    def __init__(self):
        self.props = {
            'title': None,
            'original_title': None,
            'url': None,
            'location': None,
            'mini_description': None,
            'description': None,
            'html_description': None,
            'starts_from': None,
            'salary_from': None,
            'salary_to': None,
            'logo': None,
            'company_name': None,
            'work_type': None,
            'dynamic_bid': None,
            'language': None,
            'external_id': None,
            'code': None,
        }

    def set(self, key, value):
        if hasattr(self, 'transform_%s' % key):
            transform_func = getattr(self, 'transform_%s' % key)
            val = transform_func(value)
        else:
            val = value

        self.props[key] = val

    def get(self, item):
        try:
            return self.props[item]
        except KeyError:
            raise AttributeError

    def get_insert_tuple(self, created_user_id, current_datetime):
        created_at = current_datetime
        updated_at = current_datetime

        self.update_job_offer_titles()
        self.update_job_offer_code()

        return (
            self.props['title'],
            self.props['original_title'],
            self.props['url'],
            self.props['location'],
            self.props['mini_description'],
            self.props['description'],
            self.props['html_description'],
            self.props['starts_from'],
            self.props['salary_from'],
            self.props['salary_to'],
            self.props['logo'],
            self.props['company_name'],
            self.props['work_type'],
            self.props['dynamic_bid'],
            self.props['language'],
            self.props['external_id'],
            created_user_id,
            self.props['code'],
            created_at,
            updated_at
        )

    def get_update_tuple(self, updated_at):
        self.update_job_offer_titles()
        self.update_job_offer_code()

        return (
            self.props['title'],
            self.props['original_title'],
            self.props['url'],
            self.props['location'],
            self.props['mini_description'],
            self.props['description'],
            self.props['html_description'],
            self.props['starts_from'],
            self.props['salary_from'],
            self.props['salary_to'],
            self.props['logo'],
            self.props['company_name'],
            self.props['work_type'],
            self.props['dynamic_bid'],
            self.props['language'],
            updated_at,
            self.props['code']
        )

    @staticmethod
    def transform_title(title: str):
        if title and len(title) > 190:
            return title[:187] + '...'

        return title

    @staticmethod
    def transform_description(description: str):
        if description and len(description) > 25000:
            return description[:24997] + '...'

        return description

    @staticmethod
    def transform_html_description(html_description: str):
        if html_description and len(html_description) > 25000:
            return html_description[:24997] + '...'

        return html_description

    def update_job_offer_titles(self):
        lowered_title = self.props['title'].lower()
        lowered_location = self.props['location'].lower()
        lowered_company = self.props['company_name'].lower()

        self.props['original_title'] = self.props['title']

        if lowered_company not in lowered_title:
            self.props['title'] = self.props['title'] + ' in ' + self.props['company_name']

        if lowered_location not in lowered_title:
            self.props['title'] = self.props['title'] + ' - ' + self.props['location'].capitalize()

    def update_job_offer_code(self):
        lowered_title = self.props['title'].lower()

        self.props['code'] = lowered_title\
            .strip()\
            .replace(' ', '-')\
            .replace(' ', '-')\
            .replace('.', '-')\
            .replace('%', '-')\
            .replace('|', '-')\
            .replace(',', '')\
            .replace('&', '')\
            .replace('*', '')\
            .replace('&nbsp', '-')\
            .replace('/', '-')\
            .replace('---', '-')\
            .replace('--', '-')
