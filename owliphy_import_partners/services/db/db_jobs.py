from owliphy_import_partners.services.db.db_client import DBClient


class DBJobs:

    def __init__(self):
        self.table = 'jobs_unique'
        self.client = DBClient()

    def get_job_codes_date_ordered(self, offset, limit):
        get_job_codes_query = 'SELECT jobs.id, jobs.code FROM {} AS jobs' \
            ' ORDER BY jobs.created_at DESC LIMIT {}, {}'.format(self.table, offset, limit)

        results = self.client.get_all(get_job_codes_query)
        return list(
            map(
                lambda x: '%s-%s' % (x[0], x[1]),
                results
            )
        )

    def get_job_count(self):
        get_job_count_query = 'SELECT COUNT(jobs.id) as count FROM {} AS jobs'\
            .format(self.table)

        job_count = self.client.get_one(get_job_count_query)
        if job_count:
            return job_count[0]
        else:
            return 0
