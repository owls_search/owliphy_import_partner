from owliphy_import_partners.services.db.db_client import DBClient


class DBSync:

    def __init__(self, partner_name):
        self.client = DBClient()
        self.partner_name = partner_name
        self.table = 'jobs'
        self.users_table = 'users'

        self.get_user_id_query = \
            'SELECT users.id FROM {} AS users' \
            ' WHERE users.import_partner_name = "{}"'.format(self.users_table, self.partner_name)

        self.insert_query = \
            'INSERT IGNORE INTO {} (' \
            ' title,' \
            ' original_title,' \
            ' url,' \
            ' location,' \
            ' mini_description,' \
            ' description,' \
            ' html_description,' \
            ' starts_from,' \
            ' salary_from,' \
            ' salary_to,' \
            ' logo,' \
            ' company_name,' \
            ' work_type,' \
            ' dynamic_bid,' \
            ' language,' \
            ' external_id,' \
            ' created_user_id,' \
            ' code,' \
            ' created_at,' \
            ' updated_at)' \
            ' VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'\
                .format(self.table)

    def insert(self, data_tuple):
        return self.client.insert(self.insert_query, data_tuple)

    def update(self, external_id, created_user_id, data_tuple):
        update_query = \
            'UPDATE {}' \
            ' SET' \
            ' title=%s,' \
            ' original_title=%s,' \
            ' url=%s,' \
            ' location=%s,' \
            ' mini_description=%s,' \
            ' description=%s,' \
            ' html_description=%s,' \
            ' starts_from=%s,' \
            ' salary_from=%s,' \
            ' salary_to=%s,' \
            ' logo=%s,' \
            ' company_name=%s,' \
            ' work_type=%s,' \
            ' dynamic_bid=%s,' \
            ' language=%s,' \
            ' updated_at=%s,' \
            ' code=%s,' \
            ' is_indexed=0' \
            ' WHERE external_id="{}"' \
            ' AND created_user_id={}'\
                .format(self.table, external_id, created_user_id)

        return self.client.update(update_query, data_tuple)

    def commit(self):
        return self.client.commit()

    def delete(self, external_ids, created_user_id):
        delete_query = \
            'DELETE FROM {}' \
            ' WHERE external_id IN (%s)' \
            ' AND created_user_id={}' \
                .format(self.table, created_user_id)
        query_string = delete_query % ','.join(['%s'] * len(external_ids))
        return self.client.delete(query_string, external_ids)

    def close(self):
        return self.client.close()

    def get_user_id(self):
        return self.client.get_one(self.get_user_id_query)

    def get_current_offers_external_ids(self, user_id):
        get_current_offers_external_ids_query = \
            'SELECT offers.external_id FROM {} AS offers' \
            ' WHERE offers.created_user_id={}'.format(self.table, user_id)

        results = self.client.get_all(get_current_offers_external_ids_query)
        return list(map(lambda x: x[0], results))
