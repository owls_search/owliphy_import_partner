from owliphy_import_partners.services.db.db_client import DBClient


class DBBlog:

    def __init__(self):
        self.content_table = 'posts_translates'
        self.client = DBClient()

    def get_posts_codes(self, lang):
        get_posts_codes = 'SELECT translates.code FROM {} AS translates ' \
                          'WHERE translates.language = \'{}\''.format(self.content_table, lang)

        results = self.client.get_all(get_posts_codes)
        return list(
            map(
                lambda x: x[0],
                results
            )
        )
