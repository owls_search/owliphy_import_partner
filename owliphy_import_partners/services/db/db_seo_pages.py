from owliphy_import_partners.services.db.db_client import DBClient


class DBSeoPages:

    def __init__(self):
        self.table = 'jobs_analysis_articles'
        self.client = DBClient()

    def get_seo_pages(self, lang):
        get_seo_pages = 'SELECT seo.code FROM {} AS seo ' \
                          'WHERE seo.language = \'{}\''.format(self.table, lang)

        results = self.client.get_all(get_seo_pages)
        return list(
            map(
                lambda x: x[0],
                results
            )
        )
