import os
import MySQLdb
import configparser


class DBClient:

    def __init__(self):
        config = self.get_db_config()

        self.connection = MySQLdb.connect(
            host=config['DB_HOST'],
            user=config['DB_USERNAME'],
            passwd=config['DB_PASSWORD'],
            db=config['DB_DATABASE'],
            port=int(config['DB_PORT'])
        )

        self.connection.set_character_set('utf8')

        self.cursor = self.connection.cursor()

        self.cursor.execute('SET NAMES utf8;')
        self.cursor.execute('SET CHARACTER SET utf8;')
        self.cursor.execute('SET character_set_connection=utf8;')

        self.connection.autocommit(False)

    def insert(self, query, data_tuple):
        try:
            self.cursor.execute(query, data_tuple)
        except Exception as e:
            raise RuntimeError('Failed inserting object: %s' % str(e))

    def update(self, query, data_tuple):
        try:
            self.cursor.execute(query, data_tuple)
        except Exception as e:
            raise RuntimeError('Failed updating object: %s' % str(e))

    def commit(self):
        self.connection.commit()

    def delete(self, delete_query, ids):
        try:
            self.cursor.execute(delete_query, ids)
        except Exception as e:
            raise RuntimeError('Deletion of objects failed: %s' % str(e))

    def close(self):
        self.cursor.close()
        self.connection.close()

    def get_one(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchone()

    def get_all(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def execute(self, query):
        return self.cursor.execute(query)

    def get_db_config(self):
        if self._environment_has_keys():
            return {
                'DB_HOST': os.environ.get('DB_HOST'),
                'DB_USERNAME': os.environ.get('DB_USERNAME'),
                'DB_PASSWORD': os.environ.get('DB_PASSWORD'),
                'DB_DATABASE': os.environ.get('DB_DATABASE'),
                'DB_PORT': os.environ.get('DB_PORT'),
            }

        config = configparser.ConfigParser()
        try:
            config.read('db.ini')
            return config['connection']
        except Exception as e:
            raise RuntimeError('Db config was not found: %s' % str(e))

    @staticmethod
    def _environment_has_keys():
        return 'DB_HOST' in os.environ\
               and 'DB_USERNAME' in os.environ\
               and 'DB_PASSWORD' in os.environ\
               and 'DB_DATABASE' in os.environ\
               and 'DB_PORT' in os.environ
