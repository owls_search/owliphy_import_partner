import os


class WriteFile:

    def __init__(self, partner_name, file_name):
        self.imports_dir = './import_files'
        self.current_file_size = 0
        self.buffer_size = 500 * 1024

        self.target_file = '%s/%s/new_%s' % (self.imports_dir, partner_name, file_name)
        self.final_file = '%s/%s/%s' % (self.imports_dir, partner_name, file_name)

        self.file = open(self.target_file, 'wb+')

    def write(self, buffer):
        self.current_file_size += len(buffer)
        self.file.write(buffer)

    def close(self):
        self.file.close()
        if os.path.isfile(self.final_file):
            os.remove(self.final_file)
        os.rename(self.target_file, self.final_file)

    def current_file_size(self):
        return self.current_file_size

    def buffer_size(self):
        return self.buffer_size
