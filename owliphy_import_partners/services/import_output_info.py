import os
import psutil
import time


class ImportOutputInfo:

    def __init__(self, time_start):
        super(ImportOutputInfo, self).__setattr__('dict', {
            'downloaded_mb': 0,
            'download_progress_percent': 0,
            'partner_name': '',
            'file_name': '',
            'file_size_mb': 0,

            'committed_items': 0,
            'imported_items': 0,
            'filtered_items': 0,
            'removed_items': 0,
            'updated_items': 0,
            'new_items': 0,
            'sitemaps_amount': 0,
            'files_amount': 0,

            'time_spent': 0,
            'memory_usage_mb': 0,

            'items_with_salary': '',

            'skipped': ''
        })
        super(ImportOutputInfo, self).__setattr__('time_start', time_start)
        super(ImportOutputInfo, self).__setattr__('process', psutil.Process(os.getpid()))

    def __setattr__(self, key, value):
        self.dict[key] = value

    def __getattr__(self, item):
        try:
            return self.dict[item]
        except KeyError:
            raise AttributeError

    def get_template_info(self):
        self.dict['memory_usage_mb'] = self.process.memory_info().rss / 1024 / 1024
        self.dict['time_spent'] = time.strftime('%H:%M:%S', time.gmtime(time.time() - self.time_start))
        return self.dict
