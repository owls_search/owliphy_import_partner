import time
import datetime
from dateutil import parser


class DbTimeConverter:

    @staticmethod
    def get_cur_time():
        ts = time.time()
        return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def convert(time):
        return parser.parse(time)
