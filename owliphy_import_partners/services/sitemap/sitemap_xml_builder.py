import xml.etree.cElementTree as ET
import datetime
import os


class SitemapXmlBuilder:

    def __init__(self, sitemap_folder):
        self.sitemap_folder = sitemap_folder

    def create_sitemap(self, urls, file_name, frequency='daily', priority="0.9"):
        target_file = '%s/new_%s' % (self.sitemap_folder, file_name)
        final_file = '%s/%s' % (self.sitemap_folder, file_name)

        root = ET.Element('urlset')
        root.attrib['xmlns:xsi'] = "http://www.w3.org/2001/XMLSchema-instance"
        root.attrib['xsi:schemaLocation'] = \
            "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
        root.attrib['xmlns'] = "http://www.sitemaps.org/schemas/sitemap/0.9"

        for url in urls:
            dt = datetime.datetime.now().strftime("%Y-%m-%d")
            doc = ET.SubElement(root, "url")
            ET.SubElement(doc, "loc").text = url
            ET.SubElement(doc, "lastmod").text = dt
            ET.SubElement(doc, "changefreq").text = frequency
            ET.SubElement(doc, "priority").text = priority

        tree = ET.ElementTree(root)
        tree.write(target_file, encoding='utf-8', xml_declaration=True)
        self._rename_to_final(target_file, final_file)

    def create_sitemap_index(self, sitemaps, file_name):
        target_file = '%s/new_%s' % (self.sitemap_folder, file_name)
        final_file = '%s/%s' % (self.sitemap_folder, file_name)

        root = ET.Element('sitemapindex')
        root.attrib['xmlns'] = "http://www.sitemaps.org/schemas/sitemap/0.9"

        for sitemap in sitemaps:
            sitemap_el = ET.SubElement(root, "sitemap")
            ET.SubElement(sitemap_el, "loc").text = \
                'https://clusterjobs.de/%s' % sitemap

        tree = ET.ElementTree(root)
        tree.write(target_file, encoding='utf-8', xml_declaration=True)
        self._rename_to_final(target_file, final_file)

    @staticmethod
    def _rename_to_final(temp_file, final_file):
        if os.path.isfile(final_file):
            os.remove(final_file)
        os.rename(temp_file, final_file)
