from owliphy_import_partners.services.file.google_storage import GoogleStorage
from os import walk


class SitemapToGoogle:

    def __init__(self, sitemaps_directory):
        self.sitemaps_dir = sitemaps_directory
        self.load_client = GoogleStorage('clusterjobs-sitemap-bucket')

    def load_sitemaps(self, ):
        for (_, _, sitemap_files) in walk(self.sitemaps_dir):
            for sitemap_file in sitemap_files:
                file_stream = open(
                    '%s/%s' % (self.sitemaps_dir, sitemap_file),
                    'r'
                )
                self.load_client.upload_file(file_stream.read(), sitemap_file, content_type='application/xml')
                yield
