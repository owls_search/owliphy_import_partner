import json
import math
import urllib

from owliphy_import_partners.services.db.db_blog import DBBlog
from owliphy_import_partners.services.db.db_jobs import DBJobs
from owliphy_import_partners.services.db.db_seo_pages import DBSeoPages
from .sitemap_xml_builder import SitemapXmlBuilder
import os


class SitemapsController:

    sitemap_items = 5000

    def __init__(self, sitemap_dir):
        self.search_sitemap_file = '%s/resources/sitemap_search_links.json' % os.getcwd()
        self.sitemap_dir = sitemap_dir
        self.sitemap_builder = SitemapXmlBuilder(self.sitemap_dir)

    def _build_search_sitemap(self, created_sitemaps):
        links = []
        sitemap_searches_file = 'static-stmp-srch.xml'
        with open(self.search_sitemap_file) as json_file:
            data = json.load(json_file)
            for link in data:
                links.append('https://clusterjobs.de/search/%s' % urllib.quote(link['title'], safe=''))
        self.sitemap_builder.create_sitemap(links, sitemap_searches_file, 'weekly', "0.8")
        created_sitemaps.append(sitemap_searches_file)

    def _build_blog_sitemap(self, created_sitemaps):
        db_blog = DBBlog()

        sitemap_blog_file = 'static-stmp-blog.xml'
        urls = []
        for lang in ['en', 'ru']:
            lang_urls = db_blog.get_posts_codes(lang)
            for link in lang_urls:
                if lang is 'en':
                    urls.append('https://clusterjobs.de/blog/%s' % link)
                else:
                    urls.append('https://ru.clusterjobs.de/blog/%s' % link)

        self.sitemap_builder.create_sitemap(urls, sitemap_blog_file, 'weekly', "0.9")
        created_sitemaps.append(sitemap_blog_file)

    def _build_seo_pages_sitemap(self, created_sitemaps):
        db_seo = DBSeoPages()

        sitemap_popular_file = 'static-stmp-popular.xml'
        urls = []
        for lang in ['en', 'de']:
            seo_pages_codes = db_seo.get_seo_pages(lang)
            for code in seo_pages_codes:
                if lang is 'de':
                    urls.append('https://clusterjobs.de/search/popular/%s/' % urllib.quote(code, safe=''))
                else:
                    urls.append('https://en.clusterjobs.de/search/popular/%s/' % urllib.quote(code, safe=''))

        self.sitemap_builder.create_sitemap(urls, sitemap_popular_file, 'weekly', "0.9")
        created_sitemaps.append(sitemap_popular_file)

    def _get_sitemap_amount(self):
        db_client = DBJobs()
        number_of_jobs = db_client.get_job_count()
        return math.ceil(number_of_jobs / self.sitemap_items)

    def build(self):
        db_client = DBJobs()
        created_sitemaps = []

        self._build_search_sitemap(created_sitemaps)
        self._build_blog_sitemap(created_sitemaps)
        self._build_seo_pages_sitemap(created_sitemaps)

        sitemap_amount = self._get_sitemap_amount()

        if sitemap_amount == 0:
            return

        for index in range(sitemap_amount):
            sitemap_file = 'static-stmp-%s.xml' % index
            urls = db_client.get_job_codes_date_ordered(index * self.sitemap_items, self.sitemap_items)

            self.sitemap_builder.create_sitemap(
                list(map(lambda url: 'https://clusterjobs.de/offer/%s' % url, urls)),
                sitemap_file
            )
            created_sitemaps.append(sitemap_file)
            yield

        self.sitemap_builder.create_sitemap_index(created_sitemaps, 'static-stmp.xml')


