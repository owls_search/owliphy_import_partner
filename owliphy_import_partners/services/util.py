from bs4 import BeautifulSoup

casesensitive_whitelist_title_particles = (
    'UX',
    'UI',
    'IT',
    'AI',
    'JS',
)

whitelist_title_particles = (
    'data',
    'engineer',
    'ingeniur',
    'software',
    'developer',
    'frontend',
    'backend',
    'entwickler',
    'ux ',
    'ux-',
    ' ux',
    'ui',
    'ui-',
    ' ui',
    'designer',
    ' ai ',
    'ai-',
    'machine learning',
    'java',
    'php',
    ' it ',
    ' it',
    'it-',
)


def is_it_job(title):
    is_it_job = False

    for case_sensitive_whitelist_particle in casesensitive_whitelist_title_particles:
        is_it_job = case_sensitive_whitelist_particle in title
        if is_it_job:
            return True

    for whitelist_particle in whitelist_title_particles:
        is_it_job = whitelist_particle in title.lower()
        if is_it_job:
            return True

    return is_it_job

def has_not_empty_prop(dictionary, prop_name):
    return prop_name in dictionary and dictionary[prop_name]


def get_plain_description(html_description):
    soup = BeautifulSoup(html_description, features="lxml")
    data = soup.findAll(text=True)

    def visible(element):
        if element.parent.name in ['style', 'script', '[document]', 'head', 'title', 'br']:
            return False
        return True

    plain_description = ''
    for text in filter(visible, data):
        plain_description += text

    return plain_description


def generate_mini_description(plain_description: str):
        cut_description = plain_description[:498]
        last_space = cut_description.rfind(' ')
        return cut_description[:last_space]

def line_prepender(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + content)
