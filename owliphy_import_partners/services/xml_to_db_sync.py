from owliphy_import_partners.services.db.db_sync import DBSync
from .import_output_info import ImportOutputInfo
from .db_time_converter import DbTimeConverter
from .xml_readers.partner_xml_reader import PartnerXmlReader


class XmlToDBSync:

    def __init__(self, partner_feed_reader: PartnerXmlReader, db_sync: DBSync, output_info: ImportOutputInfo):
        self.partner_feed_reader = partner_feed_reader
        self.db_sync = db_sync
        self.output_info = output_info

    def sync(self):
        created_user = self.db_sync.get_user_id()
        if not created_user:
            raise RuntimeError('Partner is not found in `users` table!')

        created_user_id = created_user[0]
        current_external_ids = self.db_sync.get_current_offers_external_ids(created_user_id)

        updated_external_ids = []

        try:
            for element in self.partner_feed_reader.read(self.output_info):
                element_external_id = element.get('external_id')
                try:
                    if element_external_id in current_external_ids:
                        self.db_sync.update(
                            element_external_id,
                            created_user_id,
                            element.get_update_tuple(DbTimeConverter.get_cur_time())
                        )

                        self.output_info.updated_items += 1
                        updated_external_ids.append(element_external_id)
                    else:
                        self.db_sync.insert(
                            element.get_insert_tuple(created_user_id, DbTimeConverter.get_cur_time())
                        )
                        self.output_info.new_items += 1

                    self.output_info.imported_items += 1
                    if self.output_info.imported_items % 400 == 0:
                        self.output_info.committed_items += 400
                        self.db_sync.commit()
                    yield
                except Exception as e:
                    self.output_info.skipped = '==================' \
                                               'Failed updating object: %s' \
                                               '==================' % str(e)

            self.remove_old(created_user_id, current_external_ids, updated_external_ids)
            self.db_sync.commit()

            self.db_sync.close()
            yield

        except Exception as e:
            self.db_sync.commit()
            self.db_sync.close()
            raise e

    def remove_old(self, created_user_id, current_ids: list, updated_ids: list):
        remove_ids = []
        for external_id in current_ids:
            if external_id and external_id not in updated_ids:
                remove_ids.append(external_id)
                self.output_info.removed_items += 1

        if remove_ids:
            self.db_sync.delete(tuple(remove_ids), created_user_id)
