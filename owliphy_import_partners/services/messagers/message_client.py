from slackclient import SlackClient


class MessageClient:

    def __init__(self, output_func):
        slack_token = 'xoxb-440077180148-572536271154-nc0RdctNO28Ar8CbX7YVB8Zk'
        self.client = SlackClient(slack_token)
        self.slack_channel = 'CGNJKCR0X'
        self.output_func = output_func

    def slack_post(self, message):
        self.client.api_call(
            "chat.postMessage",
            channel=self.slack_channel,
            text=message
        )

    def post(self, dictionary: dict, template: str, is_important: bool):
        output = self.output_func(dictionary, template)

        if is_important:
            self.slack_post(output)
