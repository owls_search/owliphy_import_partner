
from cement import Controller, ex
from cement.utils.version import get_version_banner

from owliphy_import_partners.services.file.google_storage import GoogleStorage
from owliphy_import_partners.services.sitemap.sitemap_to_google import SitemapToGoogle
from ..services.xml_readers.stellenonline_xml_reader import StellenonlinetXmlReader
from ..services.xml_readers.joblift_xml_reader import JobliftXmlReader
from ..services.xml_readers.stellenanzeigen_xml_reader import StellenanzeigenXmlReader
from ..services.xml_readers.getinit_xml_reader import GetInItXmlReader
from ..core.version import get_version
from ..services.downloader import Downloader
from ..services.import_output_info import ImportOutputInfo
from ..services.xml_to_db_sync import XmlToDBSync
from ..services.xml_readers.nuevoo_xml_reader import NeuvooXmlReader
from owliphy_import_partners.services.db.db_sync import DBSync
from ..services.messagers.message_client import MessageClient
from ..services.sitemap.sitemap_controller import SitemapsController
from ..services.xml_readers.joblift_cpa_xml_reader import JobliftCPAXmlReader
from urllib.request import Request, urlopen


import time
import traceback


VERSION_BANNER = """
Service to import owliphy partners feeds %s
%s
""" % (get_version(), get_version_banner())


class Base(Controller):
    class Meta:
        label = 'base'

        # text displayed at the top of --help output
        description = 'Service to import owliphy partners feeds'

        # text displayed at the bottom of --help output
        epilog = 'Usage: owliphy_import_partners command1 --foo bar'

        # controller level arguments. ex: 'owliphy_import_partners --version'
        arguments = [
            ### add a version banner
            ( [ '-v', '--version' ],
              { 'action'  : 'version',
                'version' : VERSION_BANNER } ),
        ]

    start_index_url = 'https://search.clusterjobs.com/index/import/?key-index=36a3e2761055314e1dd2ea45145cc135'

    def _default(self):
        """Default action if no sub-command is passed."""
        self.app.args.print_help()

    @staticmethod
    def file_download(partner: str, url: str, file: str, output_info: ImportOutputInfo, messager: MessageClient):
        output_info.file_name = file
        downloader = Downloader(url, output_info, partner, file)

        output_counter = 0

        for _ in downloader.download():
            output_counter += 1
            if output_counter % 200 == 0:
                messager.post(output_info.get_template_info(), 'download.jinja2', False)
        messager.post(output_info.get_template_info(), 'downloaded.jinja2', False)

    @ex(
        help='neuvoo',
        arguments=[
            (['--no-download'],
             {'help': 'Run import without downloading fresh feed',
              'action': 'store_true',
              'dest': 'no_download'}),
        ],
    )
    def neuvoo(self):
        dynamic_bid_url = 'http://neuvoo.com/services/feeds/generatesV3/generate.php?partner=owliphy&country=de&page=1&of=1'
        dynamic_feed_file = 'dynamic_feed.xml'

        static_bid_url = 'http://neuvoo.com/services/feeds/generatesV3/generate.php?partner=owliphy_bulk&country=de&page=1&of=1'
        static_feed_file = 'static_feed.xml'

        messager = MessageClient(self.app.render)

        output_info = ImportOutputInfo(time.time())
        output_info.partner_name = 'neuvoo'

        messager.post(output_info.get_template_info(), 'started.jinja2', True)
        try:
            if not self.app.pargs.no_download:
                self.file_download('neuvoo', dynamic_bid_url, dynamic_feed_file, output_info, messager)
                self.file_download('neuvoo', static_bid_url, static_feed_file, output_info, messager)

            xml_to_db_sync_service = XmlToDBSync(NeuvooXmlReader((static_feed_file, dynamic_feed_file)), DBSync('neuvoo'), output_info)

            output_counter = 0
            for _ in xml_to_db_sync_service.sync():
                output_counter += 1
                if output_counter % 1000 == 0:
                    messager.post(output_info.get_template_info(), 'sync.jinja2', False)

            messager.post(output_info.get_template_info(), 'synced.jinja2', True)
        except Exception as e:
            output_info.error = str(e)
            output_info.traceback = traceback.format_exc()
            messager.post(output_info.get_template_info(), 'error.jinja2', True)

    @ex(
        help='stellenanzeigen',
        arguments=[
            (['--no-download'],
             {'help': 'Run import without downloading fresh feed',
              'action': 'store_true',
              'dest': 'no_download'}),
        ],
    )
    def stellenanzeigen(self):
        bid_url = 'http://feed.stellenanzeigen.de/owliphy'
        feed_file = 'owliphy.xml'

        messager = MessageClient(self.app.render)

        output_info = ImportOutputInfo(time.time())
        output_info.partner_name = 'stellenanzeigen'

        messager.post(output_info.get_template_info(), 'started.jinja2', True)
        try:
            if not self.app.pargs.no_download:
                self.file_download('stellenanzeigen', bid_url, feed_file, output_info, messager)

            xml_to_db_sync_service = XmlToDBSync(StellenanzeigenXmlReader(feed_file), DBSync('stellenanzeigen'), output_info)

            output_counter = 0
            for _ in xml_to_db_sync_service.sync():
                output_counter += 1
                if output_counter % 1000 == 0:
                    messager.post(output_info.get_template_info(), 'sync.jinja2', False)

            messager.post(output_info.get_template_info(), 'synced.jinja2', True)
        except Exception as e:
            output_info.error = str(e)
            output_info.traceback = traceback.format_exc()
            messager.post(output_info.get_template_info(), 'error.jinja2', True)


    @ex(
        help='stellenonline',
        arguments=[
            (['--no-download'],
             {'help': 'Run import without downloading fresh feed',
              'action': 'store_true',
              'dest': 'no_download'}),
        ],
    )
    def stellenonline(self):
        bid_url = 'https://stellenonline.s3.amazonaws.com/feeds/ELdl5LbF4PW7/owliphy_premium'
        feed_file = 'owliphy.xml'
        messager = MessageClient(self.app.render)

        output_info = ImportOutputInfo(time.time())
        output_info.partner_name = 'stellenonline'

        messager.post(output_info.get_template_info(), 'started.jinja2', True)
        try:
            if not self.app.pargs.no_download:
                self.file_download('stellenonline', bid_url, feed_file, output_info, messager)

            xml_to_db_sync_service = XmlToDBSync(StellenonlinetXmlReader(feed_file), DBSync('stellenonline'), output_info)

            output_counter = 0
            for _ in xml_to_db_sync_service.sync():
                output_counter += 1
                if output_counter % 1000 == 0:
                    messager.post(output_info.get_template_info(), 'sync.jinja2', False)

            messager.post(output_info.get_template_info(), 'synced.jinja2', True)
        except Exception as e:
            output_info.error = str(e)
            output_info.traceback = traceback.format_exc()
            messager.post(output_info.get_template_info(), 'error.jinja2', True)


    @ex(
        help='joblift',
        arguments=[
            (['--no-download'],
             {'help': 'Run import without downloading fresh feed',
              'action': 'store_true',
              'dest': 'no_download'}),
        ],
    )
    def joblift(self):
        bid_url = 'http://transport.productsup.io/1224acf8a1866b3047d4/channel/198555/joblift_external.xml'
        feed_file = 'joblift_external.xml'

        messager = MessageClient(self.app.render)

        output_info = ImportOutputInfo(time.time())
        output_info.partner_name = 'joblift'

        messager.post(output_info.get_template_info(), 'started.jinja2', True)
        try:
            if not self.app.pargs.no_download:
                self.file_download('joblift', bid_url, feed_file, output_info, messager)

            xml_to_db_sync_service = XmlToDBSync(JobliftXmlReader(feed_file), DBSync('joblift'), output_info)

            output_counter = 0
            for _ in xml_to_db_sync_service.sync():
                output_counter += 1
                if output_counter % 1000 == 0:
                    messager.post(output_info.get_template_info(), 'sync.jinja2', False)

            messager.post(output_info.get_template_info(), 'synced.jinja2', True)
        except Exception as e:
            output_info.error = str(e)
            output_info.traceback = traceback.format_exc()
            messager.post(output_info.get_template_info(), 'error.jinja2', True)
    @ex(
        help='joblift_cpa',
        arguments=[
            (['--no-download'],
             {'help': 'Run import without downloading fresh feed',
              'action': 'store_true',
              'dest': 'no_download'}),
        ],
    )
    def joblift_cpa(self):
        bid_url = 'https://transport.productsup.io/ff027acf1cec2bd922a8/channel/205662/joblift_external.xml'
        feed_file = 'joblift_external.xml'

        messager = MessageClient(self.app.render)

        output_info = ImportOutputInfo(time.time())
        output_info.partner_name = 'joblift-cpa'

        messager.post(output_info.get_template_info(), 'started.jinja2', True)
        try:
            if not self.app.pargs.no_download:
                self.file_download('joblift-cpa', bid_url, feed_file, output_info, messager)

            xml_to_db_sync_service = XmlToDBSync(JobliftCPAXmlReader(feed_file), DBSync('joblift-cpa'), output_info)

            output_counter = 0
            for _ in xml_to_db_sync_service.sync():
                output_counter += 1
                if output_counter % 1000 == 0:
                    messager.post(output_info.get_template_info(), 'sync.jinja2', False)

            messager.post(output_info.get_template_info(), 'synced.jinja2', True)
        except Exception as e:
            output_info.error = str(e)
            output_info.traceback = traceback.format_exc()
            messager.post(output_info.get_template_info(), 'error.jinja2', True)

    @ex(
        help='getinit',
        arguments=[
            (['--no-download'],
             {'help': 'Run import without downloading fresh feed',
              'action': 'store_true',
              'dest': 'no_download'}),
        ],
    )
    def getinit(self):
        bid_url = 'http://www.get-in-it.de/jobsuche/feeds/owliphy.xml'
        feed_file = 'owliphy.xml'

        messager = MessageClient(self.app.render)

        output_info = ImportOutputInfo(time.time())
        output_info.partner_name = 'getinit'

        messager.post(output_info.get_template_info(), 'started.jinja2', True)
        try:
            if not self.app.pargs.no_download:
                self.file_download('getinit', bid_url, feed_file, output_info, messager)

            xml_to_db_sync_service = XmlToDBSync(GetInItXmlReader(feed_file), DBSync('getinit'),
                                                 output_info)

            output_counter = 0
            for _ in xml_to_db_sync_service.sync():
                output_counter += 1
                if output_counter % 1000 == 0:
                    messager.post(output_info.get_template_info(), 'sync.jinja2', False)

            messager.post(output_info.get_template_info(), 'synced.jinja2', True)
        except Exception as e:
            output_info.error = str(e)
            output_info.traceback = traceback.format_exc()
            messager.post(output_info.get_template_info(), 'error.jinja2', True)

    @ex(help='es-index')
    def es_index(self):
        output_info = ImportOutputInfo(time.time())
        messager = MessageClient(self.app.render)
        req = Request(self.start_index_url, headers={'User-Agent': 'Mozilla/5.0'})
        urlopen(req, timeout=60 * 30, )
        messager.post(output_info.get_template_info(), 'hit_elastic.jinja2', True)

    @ex(help='build-sitemap')
    def build_sitemap(self):
        sitemap_dir = './sitemap_files'
        output_info = ImportOutputInfo(time.time())
        messager = MessageClient(self.app.render)
        messager.post(output_info.get_template_info(), 'build_sitemap.jinja2', True)
        build_counter = 0
        loaded_counter = 0

        builder = SitemapsController(sitemap_dir)

        for _ in builder.build():
            build_counter += 1
            output_info.sitemaps_amount = build_counter
            if build_counter % 10 == 0:
                messager.post(output_info.get_template_info(), 'sitemap_part_done.jinja2', True)

        sitemaps_loader = SitemapToGoogle(sitemap_dir)
        for _ in sitemaps_loader.load_sitemaps():
            loaded_counter += 1
            output_info.sitemaps_amount = loaded_counter
            if loaded_counter % 10 == 0:
                messager.post(output_info.get_template_info(), 'sitemap_loaded.jinja2', True)
        messager.post(output_info.get_template_info(), 'sitemap_loaded.jinja2', True)

        messager.post(output_info.get_template_info(), 'sitemaps_done.jinja2', True)

    @ex(help='load-bucket')
    def load_bucket(self):
        bucket_dir = './bucket'
        bucket_name = 'owliphy-company-public'
        output_info = ImportOutputInfo(time.time())
        messager = MessageClient(self.app.render)
        google_loader = GoogleStorage(bucket_name)
        messager.post(output_info.get_template_info(), 'load_bucket_start.jinja2', False)
        output_info.files_amount = 0

        for blob in google_loader.list_files():
            google_loader.load_file(blob.name, '%s/%s' % (bucket_dir, blob.name))
            output_info.files_amount += 1
            messager.post(output_info.get_template_info(), 'load_bucket.jinja2', False)

        messager.post(output_info.get_template_info(), 'load_bucket_finish.jinja2', False)

